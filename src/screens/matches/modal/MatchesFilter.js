import * as React from 'react';

import {
  Modal, Text, View, StyleSheet,
} from 'react-native';
import { Button } from 'react-native-elements';

const styles = StyleSheet.create({
  header: {
    padding: '4% 0%',
    borderBottomColor: '#444',
    borderBottomWidth: 1,
    position: 'relative',
  },
  headerFilter: {
    color: '#85C240',
    fontSize: 17,
    flex: 1,
    width: '100%',
    textAlign: 'center',
  },
  headerClose: {
    position: 'absolute',
    right: 5,
    top: 5,
  },
});

export const MatchesFilter = (visible, close) => (
  <Modal animationType="slide" transparent={false} visible={visible}>
    <View style={styles.header}>
      <Text style={styles.headerFilter}>Filter</Text>
      <Button style={styles.headerClose} title="close" onPress={close} />
    </View>
  </Modal>
);
