import React from 'react';

import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 12,
    bottom: 12,
    display: 'flex',
    flexDirection: 'row',
    paddingRight: 12,
  },
  left: {
    flex: 3,
    flexDirection: 'column',
  },
  right: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  textSmallLeft: {
    color: '#cacaca',
    fontSize: 11,
  },
  textSmallRight: {
    color: '#fff',
    fontSize: 14,
  },
  textBold: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 2,
  },
});

export const MatchesListItemFooter = ({
  timeStarting, matchName, perPlayer, matchType,
}) => (
  <View style={styles.container}>
    <View style={styles.left}>
      <Text style={styles.textBold}>{timeStarting}</Text>
      <Text style={styles.textSmallLeft}>{matchName}</Text>
    </View>
    <View style={styles.right}>
      <Text style={styles.textBold}>{perPlayer}</Text>
      <Text style={styles.textSmallRight}>{matchType}</Text>
    </View>
  </View>
);
