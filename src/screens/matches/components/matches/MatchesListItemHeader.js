import React from 'react';

import {
  View, Text, Image, StyleSheet,
} from 'react-native';

import AvatarIMG from '../../img/avatar.jpg';

const styles = StyleSheet.create({
  players: {
    position: 'absolute',
    top: 4,
    left: 0,
    textAlign: 'center',
    color: '#fff',
    fontSize: 11,
    width: '100%',
  },
  dateWrapper: {
    position: 'absolute',
    top: '12%',
    left: -5,
    overflow: 'hidden',
    backgroundColor: '#32510b',
    borderTopRightRadius: 12,
    borderBottomRightRadius: 12,
    padding: 5,
    paddingHorizontal: 12,
  },
  date: {
    fontSize: 10,
    color: '#fff',
  },
  avatarWrapper: {
    position: 'absolute',
    right: 10,
    top: 0,
    display: 'flex',
    flexDirection: 'column',
  },
  avatarGreen: {
    width: 43,
    left: '50%',
    marginLeft: -21.5,
    height: 48,
    backgroundColor: '#32510b',
  },
  avatarCircle: {
    width: 54,
    height: 54,
    overflow: 'hidden',
    borderRadius: 128,
    borderColor: '#fff',
    borderWidth: 2,
    marginTop: -32,
  },
  ownerName: {
    marginTop: 3,
    color: '#fff',
    fontSize: 11,
  },
  ownerWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
});

export const MatchesListItemHeader = ({ date, players, ownerName }) => (
  <React.Fragment>
    <Text style={styles.players}>{players}</Text>
    <View style={styles.dateWrapper}>
      <Text style={styles.date}>{date}</Text>
    </View>
    <View style={styles.avatarWrapper}>
      <View style={styles.avatarGreen} />
      <View style={styles.avatarCircle}>
        <Image style={{ width: '100%', height: '100%' }} source={AvatarIMG} />
      </View>
      <View style={styles.ownerWrapper}>
        <Text style={styles.ownerName}>{ownerName}</Text>
      </View>
    </View>
  </React.Fragment>
);
