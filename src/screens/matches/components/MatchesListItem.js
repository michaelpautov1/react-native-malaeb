import React from 'react';

import {
  TouchableHighlight, View, Text, Image, StyleSheet,
} from 'react-native';

import StadiumIMG from '../img/stadium.jpg';

import { rh } from '../../../shared/util/sizeHelper';
import { MatchesListItemHeader } from './matches/MatchesListItemHeader';
import { MatchesListItemFooter } from './matches/MatchesListItemFooter';


const styles = StyleSheet.create({
  container: {
    position: 'relative',
    borderWidth: 5,
    borderColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: rh(6),
    marginTop: rh(6),
  },
  image: {
    flex: 1,
    width: '100%',
    height: rh(150),
  },
  playersGoing: {
    fontSize: 14,
    color: '#fff',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginLeft: -45,
    marginTop: -10,
  },
});

export const MatchesListItem = (props) => {
  const { navigate, item } = props;
  return (
    <TouchableHighlight style={styles.container} onPress={() => navigate('Match', { id: item.id })}>
      <View>
        <Image source={StadiumIMG} style={styles.image} blurRadius={1} />
        <MatchesListItemHeader {...props.item} />
        <Text style={styles.playersGoing}>players going</Text>
        <MatchesListItemFooter {...props.item} />
      </View>
    </TouchableHighlight>
  );
};
