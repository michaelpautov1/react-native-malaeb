import React from 'react';

import { View } from 'react-native';
import { Font } from 'expo';
import { LayoutHeader } from '../../shared/layout/Header';
import { MatchesList } from './list/MatchesList';

export class Matches extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isFontLoaded: false };
  }

  async componentWillMount() {
    /* eslint-disable global-require */
    await Font.loadAsync({
      black: require('../../../assets/fonts/VAGRounded-Black.otf'),
      bold: require('../../../assets/fonts/VAGRounded-Bold.otf'),
      light: require('../../../assets/fonts/VAGRounded-Light.otf'),
      thin: require('../../../assets/fonts/VAGRounded-Thin.otf'),
    });
    this.setState({ isFontLoaded: true });
  }

  render() {
    const { navigate } = this.props.navigation;
    const { isFontLoaded } = this.state;
    if (isFontLoaded) {
      return (
        <View>
          <LayoutHeader title="Matches" />
          <MatchesList navigate={navigate} />
        </View>
      );
    }
    return null;
  }
}
