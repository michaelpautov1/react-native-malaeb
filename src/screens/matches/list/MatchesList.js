import React from 'react';

import { FlatList, View, StyleSheet } from 'react-native';

import { MatchesListItem } from '../components/MatchesListItem';

import { MatchesMock } from '../mocks/MatchesMock';

const styles = StyleSheet.create({
  container: {
    marginLeft: 4,
    marginRight: 4,
    marginTop: 6,
  },
});

export const MatchesList = ({ navigate }) => (
  <View style={styles.container}>
    <FlatList
      data={MatchesMock}
      keyExtractor={(match, i) => i.toString()}
      renderItem={match => <MatchesListItem navigate={navigate} {...match} />}
    />
  </View>
);
