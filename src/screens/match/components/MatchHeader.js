import * as React from 'react';

import { StyleSheet, Image } from 'react-native';

import StadiumIMG from '../../matches/img/stadium.jpg';

const styles = StyleSheet.create({
  image: {
    flex: 1,
    width: null,
    height: '20%',
  },
});

export const MatchHeader = () => <Image source={StadiumIMG} style={styles.image} blurRadius={1} />;
