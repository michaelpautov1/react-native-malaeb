import React from 'react';

import { View } from 'react-native';

export const MatchListItem = ({ list }) => (
  <View>
    {list.map()}
  </View>
);
