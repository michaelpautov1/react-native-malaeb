import React from 'react';

import { View } from 'react-native';
import { Font } from 'expo';

import { LayoutHeader } from '../../shared/layout/Header';
import { MatchesMock } from '../matches/mocks/MatchesMock';

import { MatchHeader } from './components/MatchHeader';

export class Match extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isFontLoaded: false };
  }

  async componentWillMount() {
    /* eslint-disable global-require */
    await Font.loadAsync({
      black: require('../../../assets/fonts/VAGRounded-Black.otf'),
      bold: require('../../../assets/fonts/VAGRounded-Bold.otf'),
      light: require('../../../assets/fonts/VAGRounded-Light.otf'),
      thin: require('../../../assets/fonts/VAGRounded-Thin.otf'),
    });
    this.setState({ isFontLoaded: true });
  }

  render() {
    const { goBack, getParam } = this.props.navigation;
    const { isFontLoaded } = this.state;
    const id = getParam('id');
    const item = MatchesMock[id];
    console.log(item);
    if (isFontLoaded) {
      return (
        <View>
          <LayoutHeader goBack={goBack} title="Match" />
          <MatchHeader />
        </View>
      );
    }
    return null;
  }
}
