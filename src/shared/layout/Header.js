import React from 'react';

import { TouchableHighlight, StyleSheet } from 'react-native';
import { Header, Icon } from 'react-native-elements';

const styles = StyleSheet.create({
  centerText: {
    color: '#fff',
    fontFamily: 'light',
    fontSize: 17,
    fontWeight: '500',
  },
});

export const LayoutHeader = ({ title, goBack }) => (
  <Header
    leftComponent={goBack ? <LeftComponent goBack={goBack} /> : null}
    centerComponent={{ text: title, style: styles.centerText }}
    backgroundColor="#8bc14f"
  />
);

export const LeftComponent = ({ goBack }) => (
  <TouchableHighlight onPress={() => goBack()}>
    <Icon name="arrow-back" color="#fff" />
  </TouchableHighlight>
);
