import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const defaultPixelWidth = 320;
const defaultPixelHeight = 568;


const pixelDifferentHeight = defaultPixelHeight / height;
const pixelDifferentWidth = defaultPixelWidth / width;

export const rw = size => size * pixelDifferentHeight;
export const rh = size => size * pixelDifferentWidth;

export const vw = width / 100;
export const vh = height / 100;

export const vmin = Math.min(vw, vh);
export const vmax = Math.max(vw, vh);
