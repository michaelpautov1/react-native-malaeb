import { createStackNavigator } from 'react-navigation';

import { Matches } from './src/screens/matches/Matches';
import { Match } from './src/screens/match/Match';

const App = createStackNavigator(
  {
    Matches: { screen: Matches },
    Match: { screen: Match },
  },
  {
    initialRouteName: 'Matches',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

export default App;
