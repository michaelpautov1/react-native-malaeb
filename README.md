# React Native Malaeb


## Installation:

### Global Dependencies
```
    npm install -g expo-cli
```
### Local
```
    npm install
```

##  Development

```
    npm run start 
```

More info you can find in the [doc](https://facebook.github.io/react-native/docs/getting-started.html)